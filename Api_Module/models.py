from django.db import models
from django.contrib.auth.models import AbstractUser


class Manager_Model(models.Model):
    email=models.EmailField()
    first_name=models.CharField(max_length=20)
    last_name=models.CharField(max_length=20)
    password=models.CharField(max_length=30)
    address=models.CharField(max_length=40)
    dob=models.DateField()
    company=models.CharField(max_length=20)


class Employees(models.Model):
    email = models.EmailField()
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    password = models.CharField(max_length=100)
    address = models.CharField(max_length=40)
    dob = models.DateField()
    company = models.CharField(max_length=20)
    mobile=models.CharField(max_length=10)
    city=models.CharField(max_length=20)
