from django.shortcuts import render,HttpResponse
from rest_framework.viewsets import ModelViewSet
from .models import Employees,Manager_Model
from .serializers import Manager_Serializer,Employee_Serializer,Registration_serializer
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated


class Manager_Viewset(ModelViewSet):
    queryset = Manager_Model.objects.all()
    serializer_class = Manager_Serializer

class Employee_Viewset(ModelViewSet):
    permission_classes = []
    queryset = Employees.objects.all()
    serializer_class = Employee_Serializer


class Registration_Viewset(ModelViewSet):
    queryset = Manager_Model.objects.all()
    serializer_class = Registration_serializer



    # def post(self,request):
    #     mseri=Registration_serializer(request.data)
    #     if mseri.is_valid():
    #         mseri.save()
    #         return Response(mseri.data)
    #     return Response(mseri.errors)

# class ManagerClass(View):
#     def get(self,request):
#         return render(request,'a.html')
#
#     def post(self,request):
#         email=request.POST.get('email')
#         password=request.POST.get('password')
#         try:
#             m=Manager.objects.get(email=email)
#             return render(request,'g.html')
#
#         except:
#             print('invalid Manager')
#             return render(request,'a.html')

