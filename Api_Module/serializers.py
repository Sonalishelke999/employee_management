from rest_framework import serializers
from .models import Manager_Model,Employees
from rest_framework.validators import UniqueValidator
from django.core.exceptions import ValidationError
import re
from django.contrib.auth.hashers import make_password

class Manager_Serializer(serializers.ModelSerializer):
    class Meta:
        model=Manager_Model
        fields='__all__'

class Employee_Serializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[UniqueValidator(queryset=Employees.objects.all())])
    def validate(self, attrs):
        password = attrs.get('password')
        if len(password)<8:
            raise ValidationError("password length should be greater than 8 ")
        elif not re.findall('[A-Z]', password):
                raise ValidationError(
                    ("The password must contain at least 1 uppercase letter, A-Z."),
                    code='password_no_upper',
                )
        elif not re.findall('\d', password):
            raise ValidationError(
                ("The password must contain at least 1 digit, 0-9."),
                code='password_no_number',
            )
        elif not re.findall('[a-z]', password):
            raise ValidationError(
                ("The password must contain at least 1 lowercase letter, a-z."),
                code='password_no_lower',
            )
        elif not re.findall('[@#%]', password):
            raise ValidationError(
                ("The password must contain at least 1 symbol: " +
                  "@#%"),
                code='password_no_symbol',
            )
        attrs['password']= make_password(attrs['password'])
        print(attrs['password'])

        return attrs

    class Meta:
        model=Employees
        fields='__all__'



class Registration_serializer(serializers.ModelSerializer):
    class Meta:
        model=Manager_Model
        fields=['email','password']

        # def create(self,validated_data):
        #     user = Manager_Model.objects.create_user(validated_data['email'],
        #                                     validated_data['password'])
        #     return user
