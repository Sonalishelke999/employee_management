"""Emp_Management URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from rest_framework import routers
from Api_Module import views
from rest_framework_simplejwt.views import ( TokenObtainPairView,TokenRefreshView)
from rest_framework_swagger.views import get_swagger_view
schema_view = get_swagger_view(title='Employees Manage API')

router=routers.DefaultRouter()
router.register('employee',views.Employee_Viewset)


# router.register('Manager',views.Manager_Viewset)
# router.register('Manager/login',views.Manager_Viewset)

urlpatterns = [
    path('api_swagger/', schema_view),
    path('admin/', admin.site.urls),
    path('api/',include(router.urls)),
    path('employee/',include('Manager_Module.urls')),
    path('auth/login/',TokenObtainPairView.as_view(),name='creat_token')
    # path('a',ManagerClass.as_view())
]
