This is Employees Management Project
Follow Following Steps to run project

1. Open Editor
2. Open folder where yotu want start Project (cd folder)
3. copy the link of Clone repository and past on editor
4. run the command "pip install -r requirements.txt"(without quotes)
5. In setting.py ,Configure your Database details
   and create database in database
   
6. Migrates the models using following commands
   
   1.python manage.py makemigrations 
   
   2.python manage.py migrate
   
7. then run the project - python manage.py runserver

There Are 5 urls given as follows:
1) api_swagger/
this url is for Swagger Implementation for APIs
   
2) admin/
this url is for admin site operations
   here you have to provid credentials (username,password) ,for that you have create Superuser at terminal
   command for creating superuser: 'python manage.py createsuperuser'
   
3) api/ 
  this url is for Django rest Apis 
  
   1.api/employee 
   
   2.api/employee/id

4) auth/login/ [name='creat_token']
this url is for creating JWT Token

5) employee/
this url is for django project ORM with GUI
   here is pages are as below:
   
   1. manager login
      
   2. if manager is new user then click on new user
      you will redirect to Registration page
      
   3. After successful registration do login using
      credentials
      
   4. After login You will get Employee management
      page where you can add ,update and delete
      Employee
   
   



