from django.urls import path,include
from . import views
urlpatterns = [
    path('registration',views.register.as_view(),name='registration'),
    path('',views.login_view.as_view(),name='login'),
    path('home',views.home.as_view(),name='home'),
    path('add_employee',views.add_employee.as_view(),name='add_employee'),
    path('update_employee/<int:pk>',views.update_employee.as_view(),name='update_employee')
    # path('login',views.loginf,name='login')
]
