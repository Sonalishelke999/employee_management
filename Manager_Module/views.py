from django.shortcuts import render,redirect
from django.views import View
from .models import Manager_Model,Employees
from django.contrib import messages
from django.contrib.auth.hashers import make_password,check_password

# Create your views here.

class register(View):
    def get(self,request):
        return render(request,'registration.html')

    def post(self,request):
        email = request.POST.get('email')
        first_name =request.POST.get('firstname')
        last_name = request.POST.get('lastname')
        password = request.POST.get('password')
        address = request.POST.get('address')
        dob = request.POST.get('dob')
        company = request.POST.get('company')

        values={'fname':first_name,'lname':last_name,'company':company,'email':email,'address':address}

        # valemail = Manager_Model.objects.get(email=email)
        #
        # if valemail:
        #     messages.error(request, "Already registered email ,You can Login")
        #     return redirect('registration')
        # else:
        messages.success(request, 'Your account has been created ! You are now able to log in')
        manager_obj=Manager_Model(email=email,first_name=first_name,last_name=last_name,password=password,
                              address=address,dob=dob,company=company)
        errormsg = None

        exist = manager_obj.exist()

        if not first_name:
            errormsg = "enter first name"
        elif not last_name:
            errormsg = "enter last name"
        elif not password:
            errormsg = "enter password"
        elif len(password) < 6:
            errormsg = "enter password with atleast 6 chars"
        elif exist == True:
            errormsg = "email id exist"

        if errormsg == None:
            manager_obj.password = make_password(manager_obj.password)
            manager_obj.save()
            return redirect('login')
        else:
            return render(request,'registration.html',{'error':errormsg,'values':values})





     # print(email,first_name,last_name,password,address,dob,company)

class login_view(View):
    def get(self,request):
        return render(request,'login.html')

    def post(self,request):
         email = request.POST.get('email')
         password = request.POST.get('password')
         man=Manager_Model.objects.filter(email=email)
         manage=None
         for i in man:
             manage=i
         errormsg = None
         if man:
             ch = check_password(password,manage.password)
             if ch:
                 return redirect('home')
             else:
                 errormsg = "password is wrong"

         else:
             errormsg = "email or password is wrong"

         return render(request, 'login.html', {'error': errormsg})

         # print(email,first_name,last_name,password,address,dob,company)
         return redirect('login')

class home(View):
    def get(self,req):
        employees=Employees.objects.all()
        return render(req,'home.html' ,{"employees":employees})
    def post(self,request):
        update=request.POST.get('update')
        delete=request.POST.get('delete')

        if update:
            emp = Employees.objects.get(id=update)
            return redirect('update_employee')
        if delete:
            emp = Employees.objects.get(id=delete)
            emp.delete()
        print(update,delete)
        return redirect('home')


class add_employee(View):
    def get(self,request):
        return render(request,'add_employee.html')

    def post(self,request):
        email = request.POST.get('email')
        first_name = request.POST.get('firstname')
        last_name = request.POST.get('lastname')
        password = request.POST.get('password')
        address = request.POST.get('address')
        dob = request.POST.get('dob')
        company = request.POST.get('company')
        city = request.POST.get('city')
        mobile = request.POST.get('mobile')

        employee_obj = Employees(email=email, first_name=first_name, last_name=last_name, password=password,
                                    address=address, dob=dob, company=company,city=city,mobile=mobile)
        employee_obj.save()

        return redirect('home')


class update_employee(View):
    def get(self,request,pk):
        emp=Employees.objects.get(id=pk)
        return render(request,'update_employee.html',{'employee':emp})

    def post(self,request,pk):
        email = request.POST.get('email')
        first_name = request.POST.get('firstname')
        last_name = request.POST.get('lastname')
        password = request.POST.get('password')
        address = request.POST.get('address')
        dob = request.POST.get('dob')
        company = request.POST.get('company')
        city = request.POST.get('city')
        mobile = request.POST.get('mobile')

        employee_obj = Employees(email=email, first_name=first_name, last_name=last_name, password=password,
                                    address=address, dob=dob, company=company,city=city,mobile=mobile)
        employee_obj.id=pk
        employee_obj.save()

        return redirect('home')


