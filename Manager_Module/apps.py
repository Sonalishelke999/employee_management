from django.apps import AppConfig


class ManagerModuleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Manager_Module'
